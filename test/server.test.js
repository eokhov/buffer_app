const redisClient = require('../config/database');
const server = require( '../index');
const fetch = require('node-fetch');

describe('Server tests', () => {
  before(done => {
    server.listen(3000, done);
  })

  after(done => {
    server.close(done);
  })

  describe('Give', () => {
    it('Send string', done => {
      fetch('http://127.0.0.1:3000/give?string=qwereqw')
        .then(res => res.text())
        .then(data => console.log(data))
        .catch(err => done(err))
      done();
    });
  });
  describe('Receive', () => {
    it('Receive string', done => {
      fetch('http://127.0.0.1:3000/receive')
        .then(res => res.json())
        .then(data => console.log(data))
        .then(
          fetch('http://127.0.0.1:3000/remove')
            .then(res => res.text())
            .then(data => console.log(data))
            .catch(err => done(err))
        )
        .catch(err => done(err))
      done()
    })
  });
})