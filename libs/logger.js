const dayjs = require('dayjs');
const { app_name } = require('../config/index');

class Logger {
  info(message) {
    const meta = Logger.getMeta();
    console.log(`[${meta.time}] ${meta.appName}.INFO: ${message}`);
  }

  warning(message) {
    const meta = Logger.getMeta();
    console.log(`[${meta.time}] ${meta.appName}.WARNING: ${message}`);
  }

  error(message, error) {
    const meta = Logger.getMeta();
    console.log(`[${meta.time}] ${meta.appName}.ERROR: ${message} ${error}`);
  }

  static getMeta() {
    return {
      time: dayjs().format('YYYY-MM-DD HH:mm:ss'),
      appName: app_name
    };
  }
}

function createInstance() {
  return new Logger();
}

const logger = createInstance();

module.exports = logger;
