const result = require("dotenv").config();

module.exports = {
  app_port: process.env.APP_PORT,
  app_name: process.env.APP_NAME,

  redis_host: process.env.REDIS_HOST,
  redis_port: process.env.REDIS_PORT,
  redis_pass: process.env.REDIS_PASS,
  redis_prefix: process.env.REDIS_PREFIX,
}