const Redis = require('ioredis');
const logger = require('../libs/logger');
const config = require('./index');

const redisClient = new Redis({
  host: config.redis_host,
  port: config.redis_port,
  password: config.redis_pass,
});

redisClient.on("error", (err) => {
  logger.error("Redis client error", err);
});

redisClient.on("ready", () => {
  logger.info("Redis connected");
});

module.exports.redisClient = redisClient;
