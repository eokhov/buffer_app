const { redisClient } = require('../config/database');
const { redis_prefix } = require('../config');
const logger  = require('../libs/logger');
const { promisify } = require('util');

const rpush = promisify(redisClient.rpush).bind(redisClient);
const lrange = promisify(redisClient.lrange).bind(redisClient);
const llen = promisify(redisClient.llen).bind(redisClient);
const ltrim = promisify(redisClient.ltrim).bind(redisClient);

module.exports = {
  setString: async (string) => {
    try {
      const data = {
        string: string,
        timestamp: Date.now()
      }
      await rpush(redis_prefix, JSON.stringify(data));
      return 'Add data to redis';
    } catch (err) {
      logger.error('Add data error', err);
      return err;
    }
  },
  
  getStrings: async (limit) => {
    try {
      const keys = limit ? --limit : await llen(redis_prefix);
      const values = await lrange(redis_prefix, 0, keys);
      const result = values.map(obj => JSON.parse(obj).string);

      return result;
    } catch (err) {
      logger.error('Get err', err);
      return err;
    }
  },

  delStrings: async (limit) => {
    try {
      let keys = limit ? +limit : await llen(redis_prefix);
      const result = await ltrim(redis_prefix, 1, -keys)

      return result;
    } catch (err) {
      logger.error('Del err', err);
      return err;
    }
  }
}
