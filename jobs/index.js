const cron = require('node-cron');
const { redisClient } = require('../config/database');
const { promisify } = require('util');
const { redis_prefix } = require('../config');

const rpush = promisify(redisClient.rpush).bind(redisClient);
const lrange = promisify(redisClient.lrange).bind(redisClient);
const llen = promisify(redisClient.llen).bind(redisClient);
const ltrim = promisify(redisClient.ltrim).bind(redisClient);

const task = cron.schedule('* * * * *', async () =>  {
  const current = new Date;

  try {
    const keys = await llen(redis_prefix);
    const values = await lrange(redis_prefix, 0, keys);
    await ltrim(redis_prefix, 1, -keys);

    const result = values
      .map(string => JSON.parse(string))
      .filter(obg => obg.timestamp > current - 120000)

    for(let obj of result) {
      await rpush(redis_prefix, JSON.stringify(obj))
    }

  } catch (err) {
    console.log(err);
  }
});

module.exports = task;