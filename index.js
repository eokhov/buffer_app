const http = require('http');
const config = require('./config/index');
const logger = require('./libs/logger');
const { setString, getStrings, delStrings } = require('./service/index');
const express = require('express');
const task = require('./jobs');

const app = express();
task.start();
app.get('/give', async (req, res) => {
  const { string } = req.query;
  
  try {
    const result = await setString(string);
    res.send(result).status(200);
  } catch (err) {
    res.send('Internal server error').status(500);
  }
});

app.get('/receive', async (req, res) => {
  const { limit } = req.query;
  
  try {
    const result = await getStrings(+limit);
    res.send(result).status(200)
  } catch (err) {
    res.send('Internal server error').status(500);
  }
});

app.get('/remove', async (req, res) => {
  const { limit } = req.query;
  
  try {
    const result = await delStrings(limit);
    res.send(result).status(200);
  } catch (err) {
    console.log(err);
    res.send('Internal server error').status(500);
  }
})

const server = http.createServer(app);


server.listen(config.app_port, (err) => {
  if(err) logger.error('Server not started', err);
  else logger.info(`Server listening on port, ${config.app_port}`);
})

module.exports = server;