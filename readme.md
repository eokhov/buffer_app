## Buffer server

developed on NodeJS v14.15.0

### For starting

Install dependencies 
### `npm install`

Create a file .env by copying .env-sample filling it with your data

Run server
### `npm start`

Or run in development mode
### `npm run dev`

### Using

After starting, the server will be available at
### `http://localhost:3000`

The route is used to send data 
### `http://localhost:3000/give?string=somestring`

The route is used to receive data
### `http://localhost:3000/receive?limit=1`
Where parameter limit is optional
